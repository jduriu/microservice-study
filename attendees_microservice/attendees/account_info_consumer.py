from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO

while True:
    try:

        def update_AccountVO(ch, method, properties, body):
            content = json.loads(body)
            content["updated"] = datetime.fromisoformat(content["updated"])
            if content["is_active"]:
                AccountVO.objects.update_or_create(
                    **content,
                    defaults={"email": content["email"]},
                )
                print("Something happened")
            else:
                AccountVO.objects.filter(email=content["email"]).delete()

        def main():
            connection = pika.BlockingConnection(
                pika.ConnectionParameters(host="rabbitmq")
            )
            channel = connection.channel()

            channel.exchange_declare(
                exchange="account_info", exchange_type="fanout"
            )

            result = channel.queue_declare(queue="", exclusive=True)
            queue_name = result.method.queue

            channel.queue_bind(exchange="account_info", queue=queue_name)

            # def callback(ch, method, properties, body):
            #     print(" [x] %r" % body.decode())

            print(" [*] Waiting for logs. To exit press CTRL+C")
            channel.basic_consume(
                queue=queue_name,
                on_message_callback=update_AccountVO,
                auto_ack=True,
            )

            channel.start_consuming()

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
#   try
#       create the pika connection parameters
#       create a blocking connection with the parameters
#       open a channel
#       declare a fanout exchange named "account_info"
#       declare a randomly-named queue
#       get the queue name of the randomly-named queue
#       bind the queue to the "account_info" exchange
#       do a basic_consume for the queue name that calls
#           function above
#       tell the channel to start consuming
#   except AMQPConnectionError
#       print that it could not connect to RabbitMQ
#       have it sleep for a couple of seconds
