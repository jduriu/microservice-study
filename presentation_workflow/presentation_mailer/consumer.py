import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            print("  Received %r" % body)

            content = json.loads(body)
            presenter_name = content.get("presenter_name")
            presenter_email = content.get("presenter_email")
            title = content.get("title")

            send_mail(
                "Your presentation has been accepted",  # Subject
                f"{presenter_name}, we're happy to tell you that your presentation {title} has been accepted",  # Message
                "admin@conference.go",  # from email
                [presenter_email],  # To email
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            print("  Received %r" % body)

            content = json.loads(body)
            presenter_name = content.get("presenter_name")
            presenter_email = content.get("presenter_email")
            title = content.get("title")

            send_mail(
                "Your presentation has been rejected",  # Subject
                f"{presenter_name}, unfortunately your presentation {title} has been rejected",  # Message
                "admin@conference.go",  # from email
                [presenter_email],  # To email
                fail_silently=False,
            )

        def main():
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="presentation_rejections")
            channel.queue_declare(queue="presentation_approvals")
            # queues = ["presentation_approvals", "presentation_rejections"]
            # for queue in queues:
            #     channel.queue_declare(queue=queue)

            channel.basic_consume(
                queue="presentation_rejections",
                on_message_callback=process_rejection,
                auto_ack=True,
            )
            channel.basic_consume(
                queue="presentation_approvals",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.start_consuming()

        if __name__ == "__main__":
            try:
                main()
            except KeyboardInterrupt:
                print("Interrupted")
                try:
                    sys.exit(0)
                except SystemExit:
                    os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
